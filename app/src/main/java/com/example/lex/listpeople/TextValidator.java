package com.example.lex.listpeople;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Created by lex on 10.08.18.
 */

public abstract class TextValidator implements TextWatcher {
    private final EditText ET;

    public TextValidator(EditText et) {
        this.ET = et;

    }

    public abstract void validate (EditText tv, String text);
    @Override
    public void afterTextChanged(Editable s) {
        String text = s.toString();
        validate(ET, text);
    }
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {/*nvm*/}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {/*nvm*/}




}
