package com.example.lex.listpeople;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lex on 07.08.18.
 */

public class Person implements Parcelable {
    public enum Health {
        HEALTHY("здоров"),
        SURVEY("обследуется"),
        ILL("болен");

        private String translateRus;

        Health(String translateRus){
            this.translateRus = translateRus;
        }

        public String getTranslateRus() { return translateRus; }

    }

    private String name, company;
    private int age, index;
    private Health health;

    Person(String name,
           String company,
           int age, Health health){
        this.name = name;
        this.company = company;
        this.age = age;
        this.health = health;
    }

    private Person(Parcel parcel){
      name = parcel.readString();
      company = parcel.readString();
      age = parcel.readInt();
      health = Health.valueOf(parcel.readString());
      index = parcel.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int flags){
        parcel.writeString(name);
        parcel.writeString(company);
        parcel.writeInt(age);
        parcel.writeString(health.name());
        parcel.writeInt(index);
    }

    public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>(){
        public Person createFromParcel(Parcel in){
            return new Person(in);
        }
        public Person[] newArray (int size){
            return new Person[size];
        }
    };

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setCompany(String company) {
        this.company = company;
    }
    public String getCompany() {
        return company;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public int getAge() {
        return age;
    }

    public void setHealth(Health health) { this.health = health; }
    public Health getHealth() { return health; }

    public void setIndex(int index) { this.index = index; }
    public int getIndex() { return index; }

}
