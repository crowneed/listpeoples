package com.example.lex.listpeople;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class EditItemActivity extends AppCompatActivity {

    private static final String ERROR_NULL = "Поле должно быть заполнено";
    private static final String ERROR_AGE_OUT_LIMIT = "Возраст должен быть от 18 до 99";
    private static final String ERROR_NAME_NOT_ALPHABET = "Имя должно состоять из букв";
    private static final String TAG_AGE = "age";
    private static final String TAG_COMPANY = "company";
    private static final String TAG_NAME = "name";
    private static final String TAG_HEALTH = "healthPerson";
    private static final int AGE_MIN = 18;
    private static final int AGE_MAX = 99;
    private static boolean isValidateName = false;
    private static boolean isValidateCompany = false;
    private static boolean isValidateAge = false;
    private EditText etCompany, etAge, etName;
    private Button btnComplete;
    private Person.Health healthPerson;
    private int indexPerson;
    private Spinner spinHealth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);

        etName = findViewById(R.id.et_name);
        etCompany = findViewById(R.id.tv_company);
        etAge = findViewById(R.id.tv_age);
        spinHealth = findViewById(R.id.spin_health);
        btnComplete = findViewById(R.id.btn_complete);

        addValidators();
        init(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(TAG_HEALTH, healthPerson.name());
        outState.putString(TAG_NAME, etName.getText().toString());
        outState.putString(TAG_COMPANY, etCompany.getText().toString());
        outState.putString(TAG_AGE, etAge.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String name = savedInstanceState.getString(TAG_NAME);
        String company = savedInstanceState.getString(TAG_COMPANY);
        String age = savedInstanceState.getString(TAG_AGE);
        String healthName = savedInstanceState.getString(TAG_HEALTH);
        healthPerson = Person.Health.valueOf(healthName);

        etName.setText(name);
        etCompany.setText(company);
        etAge.setText(age);
        spinHealth.setSelection(healthPerson.ordinal());
    }

    public void addValidators(){
        etName.addTextChangedListener(new TextValidator(etName) {
            @Override
            public void validate(EditText et, String text) {
                isValidateName = isNotEmpty(et, text);
                if (isValidateName)
                    isValidateName = isAlphabetString(et, text);
                checkForUnlockButton();
            }
        });

        etCompany.addTextChangedListener(new TextValidator(etCompany) {
            @Override
            public void validate(EditText et, String text) {
                isValidateCompany = isNotEmpty(et, text);
                checkForUnlockButton();
            }
        });

        etAge.addTextChangedListener(new TextValidator(etAge) {
            @Override
            public void validate(EditText et, String text) {
                isValidateAge = isNotEmpty(et, text);
                if(isValidateAge)
                    isValidateAge = inAgeRange(et, text);
                checkForUnlockButton();
            }
        });
    }

    public boolean isNotEmpty(EditText et, String text){
        boolean result = true;
        if (text.isEmpty()) {
            et.setError(ERROR_NULL);
            result = false;
        }
        return result;
    }

    public boolean isAlphabetString(EditText et, String text){
        boolean result = true;

        for (Character ch : text.toCharArray()){
            if (!Character.isLetter(ch)) {
                result = false;
                et.setError(ERROR_NAME_NOT_ALPHABET);
                break;
            }
        }
        return result;
    }

    public boolean inAgeRange(EditText et, String text){
        boolean result = true;
        int intValue = Integer.valueOf(text);
        if (intValue < AGE_MIN
                || intValue > AGE_MAX) {
            et.setError(ERROR_AGE_OUT_LIMIT);
            result = false;
        }
        return result;
    }

    public void checkForUnlockButton(){
        btnComplete.setEnabled(
                isValidateName &&
                        isValidateCompany &&
                        isValidateAge);
    }

    public void init(Bundle savedInstanceState){
        btnComplete.setEnabled(false);
        initSpinner();

        Intent intentIn = getIntent();
        if (intentIn != null) {
            Person personIn = intentIn.getParcelableExtra(MainActivity.INTENT_PERSON);
            if (personIn != null
                    && savedInstanceState == null) {
                etName.setText(personIn.getName());
                etCompany.setText(personIn.getCompany());
                etAge.setText(String.valueOf(personIn.getAge()));
                healthPerson = personIn.getHealth();
                indexPerson = personIn.getIndex();
                spinHealth.setSelection(healthPerson.ordinal());
            }
        } else {
            healthPerson = Person.Health.HEALTHY;
            spinHealth.setSelection(healthPerson.ordinal());
        }

    }

    public void initSpinner(){
        String translates[] = new String[Person.Health.values().length];
        for (int i = 0; i < translates.length; i++) {
            translates[i] = Person.Health.values()[i].getTranslateRus();
        }

        ArrayAdapter<String> spinAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item,
                translates);
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinHealth.setAdapter(spinAdapter);

        spinHealth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                healthPerson = Person.Health.values()[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}});
    }

    public void onClick(View view) {
        Person personOut = new Person(
                etName.getText().toString(),
                etCompany.getText().toString(),
                Integer.valueOf(etAge.getText().toString()),
                healthPerson);
        personOut.setIndex(indexPerson);

        Intent intent = new Intent();
        intent.putExtra(MainActivity.INTENT_PERSON, personOut);
        setResult(RESULT_OK, intent);
        finish();
    }

}
