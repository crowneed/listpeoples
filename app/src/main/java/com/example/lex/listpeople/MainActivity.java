package com.example.lex.listpeople;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String LIST_PERSONS = "listPersons";
    private ArrayList<Person> persons = new ArrayList<>();
    private RecyclerViewAdapter rwAdapter;
    static final String INTENT_PERSON = "person";
    private final int REQ_CODE_EDIT = 1;
    private final int REQ_CODE_CREATE = 2;
    private final int PERSONS_SIZE = 30;
    private final int PERSON_AGE_MIN = 25;
    private final String PERSON_NAME = "Person";
    private final String PERSON_COMPANY = "Company ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null)
        generatePersons(persons);

        RecyclerView recyclerView = findViewById(R.id.rv);
        rwAdapter = new RecyclerViewAdapter(persons, new RecyclerViewAdapter.OnPersonClickListener() {
            @Override
            public void onPersonClick(Person person) {
                Intent intent = new Intent(MainActivity.this,
                                                            EditItemActivity.class);
                intent.putExtra(INTENT_PERSON, person);
                startActivityForResult(intent, REQ_CODE_EDIT);
            }
        });
        LinearLayoutManager Manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(Manager);
        recyclerView.setAdapter(rwAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, Manager.getOrientation()));

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        persons = savedInstanceState.getParcelableArrayList(LIST_PERSONS);
        rwAdapter.setPersons(persons);
        rwAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(LIST_PERSONS, persons);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Person personIn = data.getParcelableExtra(INTENT_PERSON);
            switch (requestCode) {
                case REQ_CODE_EDIT:
                    Person person = persons.get(personIn.getIndex());
                    person.setName(personIn.getName());
                    person.setCompany(personIn.getCompany());
                    person.setAge(personIn.getAge());
                    person.setHealth(personIn.getHealth());
                    rwAdapter.notifyItemChanged(person.getIndex());
                    break;
                case REQ_CODE_CREATE:
                    persons.add(personIn);
                    int lastIndex = persons.size() - 1;
                    persons.get(lastIndex).setIndex(lastIndex);
                    rwAdapter.notifyItemInserted(lastIndex);
                    break;
            }
        }
    }


    public void onCreateClick(View view) {
        Intent intent = new Intent(MainActivity.this,
                                                    EditItemActivity.class);
        startActivityForResult(intent, REQ_CODE_CREATE);
    }

    private void generatePersons(List<Person> persons) {
        for (int i = 0; i < PERSONS_SIZE; i++) {
            Person person = new Person(
                    PERSON_NAME,
                    PERSON_COMPANY + i,
                    PERSON_AGE_MIN + i,
                    Person.Health.values()[i%3]);
            persons.add(person);
        }
    }
}
