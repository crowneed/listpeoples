package com.example.lex.listpeople;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by lex on 07.08.18.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    public interface OnPersonClickListener{
        void onPersonClick(Person person);
    }

    private List<Person> persons;
    private final OnPersonClickListener listener;

    RecyclerViewAdapter(List<Person> persons, OnPersonClickListener listener){
        this.persons = persons;
        this.listener = listener;
    }

    void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.rw_item,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Person person = persons.get(position);
        person.setIndex(holder.getAdapterPosition());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPersonClick(persons.get(holder.getAdapterPosition()));
            }
        });
        holder.name.setText(person.getName());
        holder.company.setText(person.getCompany());
        holder.age.setText(String.valueOf(person.getAge()));
        holder.icon.setImageResource(R.drawable.ic_health);
        switch (person.getHealth()){
           case ILL:
               holder.setIconColor(R.color.color_ill);
               break;
            case HEALTHY:
               holder.setIconColor(R.color.color_healthy);
               break;
           case SURVEY:
               holder.setIconColor(R.color.color_survey);
               break;
       }
    }

    @Override
    public int getItemCount() { return persons.size(); }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView name, company, age;
        private ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tv_name);
            company = itemView.findViewById(R.id.tv_company);
            age = itemView.findViewById(R.id.tv_age);
            icon = itemView.findViewById(R.id.iv_health);
        }
        public void setIconColor(int color){
            DrawableCompat.setTint(icon.getDrawable(),
                    ContextCompat.getColor(icon.getContext(), color));
//            icon.setColorFilter(icon
//                    .getContext()
//                    .getResources()
//                    .getColor(color),
//                    PorterDuff.Mode.SRC_ATOP);
        }
    }
}
